import sources
import sinks

def main():
    fuente = str(input("\nSeleccione una fuente (load, sin, constant, square): "))
    #Para fuente

    repeticion = True #Creamos un buleano para que pueda funcionar si no se mete en la opcion corrrecta
    while repeticion :
        #Para que sea posible el bucle while, esto tiene que estar dentro

        if fuente == 'load' :
            respuesta = False
            while not respuesta:
                try:
                    path = str(input(f"\nSeleccione el path de la fuente {fuente}: "))
                    sound = sources.load(path) #Llamar a la funcion load que esta situada en sources
                except:
                    print("Alguno de los dos valores no corresponde con su clase ")
            repeticion = False
        elif fuente == 'sin':
            respuesta = False

            while not respuesta:
                try:

                    nsamples =int(input("Numero de samples: " ))
                    freq = float(input("Frecuencia: "))

                    sound = sources.sin(nsamples,freq)
                    respuesta = True

                except:
                    print("Alguno de los dos valores no corresponde con su clase: ")




            repeticion = False


        elif fuente == 'constant':
            respuesta = False
            while not respuesta:
                try:
                    nsamples = int(input(f"Introduce el numero de muestras de la fuente {fuente}: "))
                    level = int(input(f"Introduce el nivel de cada una de las muestras de la fuente {fuente}: "))
                    sound = sources.constant(nsamples,level)
                    respuesta = True
                except:
                    print("Alguno de los dos valores no corresponde con su clase: ")


            repeticion = False


        elif fuente == 'square':
            respuesta = False
            while not respuesta:

                try:
                    nsamples = int(input(f"Introduce el numero de muestras de la fuente {fuente}: "))
                    period = int(input(f"Introduce el periodo de la fuente {fuente}: "))
                    sound = sources.square(nsamples,period)
                    respuesta = True

                except:
                    print("Alguno de los dos valores no corresponde con su clase: ")

            repeticion = False

        else:
            fuente = str(input("\nSeleccione una fuente (load, sin, constant, square): "))

        # Para sumidero
    sumidero = str(input("Seleccione un sumidero (play, draw, show, info): "))

    while not repeticion:

        if sumidero == 'play':
            sinks.play(sound)
            break

        elif sumidero == 'draw':
            respuesta = False
            while not respuesta:
                try:
                    max_chars = int(input(f"Introduzca el valor máximo para el sumidero {sumidero}: "))
                    sinks.draw(sound, max_chars)
                except:
                    print("Alguno de los dos valores no corresponde con su clase: ")
            break

        elif sumidero == 'show':
            respuesta = True
            while respuesta:
                try:
                    newline = str(input(f"Indique si es 'True'o 'False' para el sumidero {sumidero}: "))
                    if newline == 'True':
                        newline= True
                        sinks.show(sound, newline)
                        respuesta = False
                    if newline == 'False':
                        newline = False
                        sinks.show(sound, newline)
                        respuesta = False

                except:
                    print("Alguno de los dos valores no corresponde con su clase: ")
            break

        elif sumidero == 'info':
            sinks.info(sound)


            break

        else:
            sumidero = str(input("Seleccione un sumidero: "))

if __name__ == '__main__':
    main()
