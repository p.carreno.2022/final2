import sources
import processors
import sinks

def main():
    fuente = str(input("Seleccione una fuente (load, sin, constant, square): "))
    #Para fuente

    repeticion = 0#Creamos un buleano para que pueda funcionar si no se mete en la opcion corrrecta
    while repeticion == 0 :
        #Para que sea posible el bucle while, esto tiene que estar dentro

        if fuente == 'load' :
            path = str(input(f"Seleccione el path de la fuente {fuente}: "))
            sound = sources.load(path) #Llamar a la funcion load que esta situada en sources
            repeticion = 1
        elif fuente == 'sin':
            nsamples =int(input(f"Seleccione el numero de muestras de la fuente {fuente}: " ))
            freq = float(input(f"Seleccione una frecuencia de la fuente {fuente}: "))
            sound = sources.sin(nsamples,freq)
            repeticion = 1


        elif fuente == 'constant':
            nsamples = int(input(f"Introduce el numero de muestras de la fuente {fuente}: "))
            level = int(input(f"Introduce el nivel de cada una de las muestras de la fuente {fuente}: "))
            sound = sources.constant(nsamples,level)
            repeticion = 1

        elif fuente == 'square':
            nsamples = int(input(f"Introduce el numero de muestras de la fuente {fuente}: "))
            period = int(input(f"Introduce el periodo de la fuente {fuente}: "))
            sound = sources.square(nsamples,period)
            repeticion = 1

        else:
            fuente = str(input("Seleccione una fuente: "))

        #Para procesador

    procesador = str(input("Seleccione un procesador(ampli, shift, trim, repeat, clean , round): "))

    while repeticion == 1:
            if procesador == 'ampli':
                factor = float(input("Seleccione un factor: "))
                processors.ampli(sound,factor)
                repeticion =2

            if procesador == 'shift':
                value = int(input("Seleccione un valor: "))
                processors.shift(sound,value)
                repeticion = 2

            if procesador == 'trim':
                reduction = int(input("Selecciona la cantidad de numeros que desea de reducir: "))
                start = bool(input("Ingrese el buleano por pantalla: "))
                processors.trim(sound,reduction,start)
                repeticion = 2

            if procesador == 'repeat':
                factor = float(input("Seleccione un factor: "))
                sound.repeat(sound,factor)
                repeticion = 2


    # Para sumidero
    sumidero = str(input("Seleccione un sumidero (play, draw, show, info): "))

    while repeticion == 2:
        if sumidero == 'play':
            sinks.play(sound)
            break

        elif sumidero == 'draw':
            max_chars = int(input(f"Introduzca el valor máximo para el sumidero {sumidero}: "))
            sinks.draw(sound, max_chars)
            break

        elif sumidero == 'show':
            newline = str(input(f"Indique si es 'True'o 'False' para el sumidero {sumidero}: "))
            if newline == 'True':
                newline= True
            if newline == 'False':
                newline = False
            sinks.show(sound, newline)
            break
        elif sumidero == 'info':
            sinks.info(sound)


            break

        else:
            sumidero = str(input("Seleccione un sumidero: "))

if __name__ == '__main__':
    main()
