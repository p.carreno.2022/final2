<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE abiword PUBLIC "-//ABISOURCE//DTD AWML 1.0 Strict//EN" "http://www.abisource.com/awml.dtd">
<abiword template="false" xmlns:ct="http://www.abisource.com/changetracking.dtd" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:svg="http://www.w3.org/2000/svg" xid-max="41" xmlns:dc="http://purl.org/dc/elements/1.1/" styles="unlocked" fileformat="1.1" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:awml="http://www.abisource.com/awml.dtd" xmlns="http://www.abisource.com/awml.dtd" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.9.1" xml:space="preserve" props="dom-dir:ltr; document-footnote-restart-section:0; document-endnote-type:numeric; document-endnote-place-enddoc:1; document-endnote-initial:1; lang:es-ES; document-endnote-restart-section:0; document-footnote-restart-page:0; document-footnote-type:numeric; document-footnote-initial:1; document-endnote-place-endsection:0">
<!-- ======================================================================== -->
<!-- This file is an AbiWord document.                                        -->
<!-- AbiWord is a free, Open Source word processor.                           -->
<!-- More information about AbiWord is available at http://www.abisource.com/ -->
<!-- You should not edit this file by hand.                                   -->
<!-- ======================================================================== -->

<metadata>
<m key="abiword.date_last_changed">Wed Jun 14 23:36:06 2023
</m>
<m key="abiword.generator">AbiWord</m>
<m key="dc.creator">Pablo Carreno Mateo</m>
<m key="dc.date">Wed Jun 14 23:36:06 2023
</m>
<m key="dc.format">application/x-abiword</m>
</metadata>
<rdf>
</rdf>
<history version="1" edit-time="47" last-saved="1686778566" uid="5c4eba86-0afb-11ee-94e4-b1b21ba35f84">
<version id="1" started="1686778566" uid="782054ae-0afb-11ee-94e4-b1b21ba35f84" auto="0" top-xid="41"/>
</history>
<styles>
<s type="P" name="Normal" followedby="Current Settings" props="font-family:Times New Roman; margin-top:0pt; color:000000; margin-left:0pt; text-position:normal; widows:2; font-style:normal; text-indent:0in; font-variant:normal; font-weight:normal; margin-right:0pt; font-size:12pt; text-decoration:none; margin-bottom:0pt; line-height:1.0; bgcolor:transparent; text-align:left; font-stretch:normal"/>
</styles>
<lists>
<l id="1" parentid="0" type="5" start-value="0" list-delim="%L" list-decimal="."/>
<l id="2" parentid="0" type="5" start-value="0" list-delim="%L" list-decimal="."/>
</lists>
<pagesize pagetype="A4" orientation="portrait" width="210.000000" height="297.000000" units="mm" page-scale="1.000000"/>
<section xid="40" props="page-margin-footer:0.5000in; page-margin-header:0.5000in; page-margin-right:1.0000in; page-margin-left:1.0000in; page-margin-top:1.0000in; page-margin-bottom:1.0000in">
<p style="Normal" xid="41"><c>Pablo Carreño Mateo </c></p>
<p style="Normal" xid="36"><c></c></p>
<p style="Normal" xid="37"><c></c></p>
<p style="Normal" xid="35"><c></c><c>REQUISITOS MÍNIMOS:</c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="1" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="2"></field><c type="list_label">	Metodo constant</c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="3" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c type="list_label"></c><field type="list_label" xid="4"></field><c>	Metodo square</c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="5" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="6"></field><c>	Metodo show</c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="7" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="8"></field><c>	Metodo info</c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="9" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="10"></field><c>	Metodo shift</c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="11" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="12"></field><c>	Metodo trim</c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="13" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="14"></field><c>	Metodo repeat</c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="15" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="16"></field><c>	Creacion programa sound.py</c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="17" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="18"></field><c>	Creacion programa menu.py</c></p>
<p level="0" listid="0" parentid="0" style="Normal" xid="38" props="list-delim:%L; list-decimal:.; list-style:Bullet List; start-value:0; margin-left:0.50in; text-indent:0.0000in; field-font:NULL"><c props="list-tag:1003"></c></p>
<p level="1" listid="1" parentid="0" style="Normal" xid="19" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c type="list_label" props="list-tag:1000"></c><field type="list_label" xid="32" props="width:0in; font-family:Times New Roman; display:inline; font-style:normal; font-weight:normal; bgcolor:transparent; lang:es-ES; text-position:normal; text-transform:none; homogeneous:1; color:000000; font-variant:normal; text-decoration:none; font-size:12pt; height:0in; list-style:None; font-stretch:normal"></field><c type="list_label">REQUISITOS OPCIONALES:</c></p>
<p level="1" listid="2" parentid="0" style="Normal" xid="20" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c type="list_label"></c><field type="list_label" xid="21"></field><c>	Metodo clean</c></p>
<p level="1" listid="2" parentid="0" style="Normal" xid="22" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="23"></field><c>	Metodo round</c></p>
<p level="1" listid="2" parentid="0" style="Normal" xid="24" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="25"></field><c>	Creacion de menu2</c></p>
<p level="1" listid="2" parentid="0" style="Normal" xid="26" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="27"></field><c>	Creacion de menu3</c></p>
<p level="1" listid="2" parentid="0" style="Normal" xid="28" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c></c><field type="list_label" xid="29"></field><c>	Metodo add</c></p>
<p level="1" listid="2" parentid="0" style="Normal" xid="30" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c type="list_label" props="list-tag:1001"></c></p>
<p level="1" listid="2" parentid="0" style="Normal" xid="31" props="start-value:0; text-indent:-0.3in; list-style:Bullet List; field-font:NULL; margin-left:0.50in"><c type="list_label" props="list-tag:1002"></c></p>
</section>
</abiword>
