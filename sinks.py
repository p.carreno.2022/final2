"""Methods for storing sound, or playing it"""

import array
import config

import sounddevice


def play(sound):
    sound_array = array.array('h', [0] * len(sound))
    for nsample in range(len(sound)):
        sound_array[nsample] = sound[nsample]
    sounddevice.play(sound_array, config.samples_second)
    sounddevice.wait()

def draw(sound, max_chars: int):
    total_chars = max_chars * 2
    for nsample in range(len(sound)):
        if sound[nsample] > 0:
            print(' ' * max_chars, end='')
            nchars = int(max_chars * (sound[nsample] / config.max_amp))
            print('*' * nchars)
        else:
            nchars = int(max_chars * (-sound[nsample] / config.max_amp))
            print(' ' * (max_chars - nchars), end='')
            print('*' * nchars)

def show(sound, newline: bool):
    muestras = [] # Creamos una lista vacia en la que si es mentira pues la rellenamos con los valores de sound
    if newline == True:
        for i in (sound):#tambien podriamos hacer un rage(len())... Pero necesitariamos una lista para ello
            print(i)
    else:
        muestras.append(sound)#Si le pusieramos un bucle for, nos imprimiria la lista que queremos tantas veces como elementos que habria en la lista
        print(muestras)
    return(sound)



def info(sound):
    samples = 0
    for i in range(len(sound)):
        samples = i +1
    contpos = 0  # contador de numeros de positivos
    contneg = 0  # contador de numeros de negativos
    cont0 = 0  # contador de numeros de nulos

    # Hacemos esto para crear una lista de 0 y que no falle cuando no halla muestras positivas o negativas

    listaPos = []
    listaNeg = []

    for i in range(len(sound)):



        if (sound[i]) > 0:
            contpos = contpos + 1  # Ponemos un contador que cada vez que pasa por un numrto positivo sume 1
            listaPos.append(sound[i])

        elif (sound[i]) < 0:
            contneg = contneg + 1
            listaNeg.append(sound[i])
        elif (sound[i]) == 0:
            cont0 = cont0 + 1

    listaNeg.sort()  # El .sort() nos permite ordenar la lista de menor a mayor
    listaPos.sort(
        reverse=True)  # Al poner el reverse true, hacemos que una vez ordenada de menor a mayor se de la vuelta
    if listaNeg == []:
        listaNeg = listaPos

    if listaPos == []:
        listaPos =listaNeg

    total = 0
    for i in sound:
        total = i + total
    media = total / samples

    print("Samples = ", samples)
    print("Max Value = ", listaPos[0])  # El lista [0] imprime el valor que se encuentra en la posición 0
    print("Min Value = ", listaNeg[0])
    print("Mean value = ", media)
    print("Positive samples = ", contpos)
    print("Negative samples = ", contneg)
    print("Null samples = ,", cont0)
