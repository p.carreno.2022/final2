import sources
import sinks

def main():
    fuente = str(input("Seleccione una fuente (load, sin, constant, square): "))
    #Para fuente

    repeticion = True #Creamos un buleano para que pueda funcionar si no se mete en la opcion corrrecta
    while repeticion :
        #Para que sea posible el bucle while, esto tiene que estar dentro

        if fuente == 'load' :
            path = str(input(f"Seleccione el path de la fuente {fuente}: "))
            sound = sources.load(path) #Llamar a la funcion load que esta situada en sources
            repeticion = False
        elif fuente == 'sin':
            nsamples =int(input(f"Seleccione el numero de muestras de la fuente {fuente}: " ))
            freq = float(input(f"Seleccione una frecuencia de la fuente {fuente}: "))
            sound = sources.sin(nsamples,freq)
            repeticion = False


        elif fuente == 'constant':
            nsamples = int(input(f"Introduce el numero de muestras de la fuente {fuente}: "))
            level = int(input(f"Introduce el nivel de cada una de las muestras de la fuente {fuente}: "))
            sound = sources.constant(nsamples,level)
            repeticion = False

        elif fuente == 'square':
            nsamples = int(input(f"Introduce el numero de muestras de la fuente {fuente}: "))
            period = int(input(f"Introduce el periodo de la fuente {fuente}: "))
            sound = sources.square(nsamples,period)
            repeticion = False

        else:
            fuente = str(input("Seleccione una fuente: "))

        # Para sumidero
    sumidero = str(input("Seleccione un sumidero (play, draw, show, info): "))

    while not repeticion:
        if sumidero == 'play':
            sinks.play(sound)
            break

        elif sumidero == 'draw':
            max_chars = int(input(f"Introduzca el valor máximo para el sumidero {sumidero}: "))
            sinks.draw(sound, max_chars)
            break

        elif sumidero == 'show':
            newline = str(input(f"Indique si es 'True'o 'False' para el sumidero {sumidero}: "))
            if newline == 'True':
                newline= True
            if newline == 'False':
                newline = False
            sinks.show(sound, newline)
            break
        elif sumidero == 'info':
            sinks.info(sound)


            break

        else:
            sumidero = str(input("Seleccione un sumidero: "))

if __name__ == '__main__':
    main()
