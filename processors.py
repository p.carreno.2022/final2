"""Methods for processing sound (sound processors)"""

import math

import soundfile

import config

def ampli(sound, factor: float):
    """Amplify signal (sound) by the specified factor

    :param sound:  sound (list of samples) to amplify
    :param factor: amplification factor
    :return:     list of samples (sound)
    """

    sound = [0] * len(sound)
    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp
        elif value < - config.max_amp:
            value = - config.max_amp
        sound[nsample] = int(value)
    print(sound)
    return sound

def shift(sound, value : int):
    lista = []
    for i in sound :
        a = i + value
        if a > config.max_amp:
            a = config.max_amp
        elif a < -config.max_amp:
            a = -config.max_amp
        lista.append(a)
    sound = lista
    print(sound)
    return lista


def trim(sound,reduction: int,start : bool):
    if reduction > len(sound):
        print("La cantidad de números que desea quitar es más grande que la cantidad de elementos de la lista")
        exit()
    if start == True :
        for i in range(reduction): #Hacemos un bucle que recorra el numero de muestras y eliminamos siempre la primera
            sound.pop(0)  #.pop() lo que hace es elimiar un elemento de la lista

    else:
        for i in  range(reduction):
            sound.pop(-1) #Con el -1 se quita la ultima muestra, en los casos anteriores "i" lo unico que hace es recorrer el bucle
    print(sound)
    return sound


def repeat(sound, factor : int):
    print(sound)
    return (factor +1) * sound

def clean(sound,level):
    lista=[]
    for i in sound:
        if (abs(i)) < (abs(level)):
            lista.append(0)
        else:
            lista.append(i)
    return (lista)

def round(sound):

    lista=[]
    for i in range(len( sound)):
        if i == 0:
            a = sound[0]
            lista.append(a)
        elif i == len(sound) - 1:
            b = sound[-1]
            lista.append(b)
        else:
            mas_uno = sound[i+1]
            menos_uno = sound[i-1]

            c = int((sound[i] + mas_uno + menos_uno)/3)
            lista.append(c)

    return (sound)


def add (sound1,sound2):
    elementos1 = 0
    elementos2 = 0
    sound = []
    for i in range(len(sound1)):
        elementos1 = i+1
    for i in range (len(sound2)):
        elementos2 = i+1


    if elementos1 > elementos2:

        for i in range(elementos1-elementos2):
            sound2.append(0)


        for i in range(len(sound1 )):


            if sound1[i] + sound2[i] > config.max_amp:
                sound.append(config.max_amp)
            elif sound1[i] + sound2[i] < -config.max_amp:
                sound.append(-config.max_amp)
            else:
                sound.append(sound1[i] + sound2[i])

    elif elementos2 > elementos1:

        for i in range(elementos2-elementos1):
            sound1.append(0)
        for i in range(len(sound2 )):
            if sound1[i] + sound2[i] > config.max_amp:
                sound.append(config.max_amp)
            elif sound1[i] + sound2[i] < -config.max_amp:
                sound.append(-config.max_amp)
            else:
                sound.append(sound1[i] + sound2[i])

    else:
        for i in range(len(sound1 )):


            if sound1[i] + sound2[i] > config.max_amp:
                sound.append(config.max_amp)
            elif sound1[i] + sound2[i] < -config.max_amp:
                sound.append(-config.max_amp)
            else:
                sound.append(sound1[i] + sound2[i])
    print(sound)
    return sound